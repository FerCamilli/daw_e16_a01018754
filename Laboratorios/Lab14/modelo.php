<?php

function connect_db() {
    $mysql = mysqli_connect("localhost","fercamilli","","lab14");
    return $mysql;
}

function close_db($mysql) {
    mysqli_close($mysql);
}

function  getFruta() {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM fruta';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        // use of numeric index
        echo 'Fruta: '. $row[1]; 
        // name of the column as associative index
    	echo ' ' .  $row['color'];
    	echo ' (' .  $row['tamano'];
    	echo '<br />';
    }
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}


function  getFrutaPorNombre($nombre) {
    $mysql = connect_db();
    
    //Specification of the SQL query
    $query = 'SELECT * FROM  fruta WHERE nombre LIKE  "'.$nombre.'"';
     // Query execution; returns identifier of the result group
    $results = $mysql->query($query);
    
    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
        // use of numeric index
        echo 'Fruta: '. $row[1]; 
        // name of the column as associative index
    	echo ', ' .  $row['color'];
    	echo ', ' .  $row['tamano'];
    	echo '<br />';
    }
    
    // it releases the associated results
    mysqli_free_result($results);
    
    close_db($mysql);
}

?>