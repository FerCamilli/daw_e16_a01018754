<?php
require_once('modelo.php');

include('_header.html');
?>

<h1>Jugadores</h1>
<div class="container">

<?php getFruta(); ?>

<?php getFrutaPorNombre('Manzana'); ?>

</div>

<div class="row">
  <div class="col s12 5">
    <div class="card-panel teal">
      <span class="white-text">
        
        <h5>Preguntas</h5>
        <p>¿Qué es ODBC y para qué es útil?</p>
            <p>Es un estandar de conexion a bases de datos utilizados principalmente por los sistemas Microsoft. Este permite a cualquier sistema de Microsoft 
            conectarse a cualquier base de datos que cuente con el driver de OBDC.</p></br>
        <p>¿Qué es SQL Injection? </p>
            <p>SQL Injection es una vulnerabilidad dentro de una base de datos por la cual un atacante realiza consultas a dicha base de datos a traves de 
            un incorrecto filtrado de la información que se pasa a través de los campos o variables en una pagina web. Este ataque le da acceso al atacante a la 
            base de datos para hacer lo que quiera con la informacion que se encuentra ahi.</p></br>
        <p>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection? </p>
            <p>La pagina "php.net" nos dice algunas tecnicas para evitar este ataque:  Nunca se conecte como superusuario o como propietario de la base de datos. 
            Siempre utilice usuarios personalizados con privilegios muy limitados. Emplee sentencias preparadas con variables vinculadas. 
            Son proporcionadas por PDO, MySQLi y otras bibliotecas.Compruebe si la entrada proporcionada tiene el tipo de datos previsto. 
            PHP tiene un amplio rango de funciones para validar la entrada de datos, desde las más simples, encontradas en Funciones de variables y en Funciones 
            del tipo carácter (p.ej., is_numeric(), ctype_digit() respectivamente), hasta el soporte para Expresiones regulares compatibles con Perl. 
            Si la expresión espera una entrada numérica, considere verificar los datos con la función ctype_digit(), o silenciosamente cambie su tipo 
            utilizando settype(), o emplee su representación numérica por medio de sprintf(). </p></br>

      </span>
    </div>
  </div>
</div>


<?php
include('_footer.html');
?>