<?php include_once("_header.html"); ?>

<?php include_once("lib.php"); ?>

<div class="container">
    
    <h1>Lab 8</h1>
    <h5>Funcion 1</h5>
    <?php echo mean($arr2) ?></br>
    <h5>Funcion 2 </h5>
    <?php echo median($arr2) ?></br>
    
    <h5>Funcion 3</h5>
    <?php echo table($arr2) ?></br>
    
    <h5>Funcion 4</h5>
    <table class="striped">
    
    <thead>
        <tr>
            <th>Numero</td>
            <th>Al Cuadrado</td>
            <th>Al Cubo</td>
        </tr> 
    </thead>    
    <tbody>
    <?php for ($i = 0; $i < 10; $i++): ?>
        <tr>
            <td><?= $i;?></td>
            <td><?= $i*$i;?></td>
            <td><?= $i*$i*$i;?></td>
        </tr>
    <?php endfor ; ?>
     </tbody>
    </table> 
    
    <h5>Funcion Extra</h5>
    <p>Calculo del area de un circulo</p>
    <p>Area: 
    <?php echo area($rad) ?></br>
    </p>
    
    <h5>Preguntas</h5>
    <p>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</p>
        <p>Esta funcion te muestra la configuracion de php que estas utilizando. Me llamo la atencion la informacion acerca de la version cliente API de mysql,
        informacion acerca de la sesion como session.cache_expire y el estado y valor de las variables de PHP.</p></br>
    <p>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</p>
        <p>Se tiene que establecer el nivel de seguridad adecuado e instalar un administrador de bases de datos.</p></br>
    <p>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</p>
        <p>Es debido a que tiene codigo de lenguaje php incrustado. Php se ejecuta en el servidor web antes de que se envíe la página a través de Internet al cliente.
        De este modo hace todas las interacciones necesarias para crear la pagina final que vera el usuario. De este modo el cliente recibe la pagina unicamente
        con el código HTML que resulta de ejecutar el php.</p></br>

</div>   

<?php include_once("_footer.html"); ?>