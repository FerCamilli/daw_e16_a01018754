<?php

$rad=3;

$arr=array(
         0 => 10,
         1 => 21,
         2 => 34,
         3 => 46,
         4 => 48
    );
    
$arr2=array(
         25,
         46,
         2,
         18,
         35
    );

function mean($arra) {
    $numMean=0;
    
    $size=sizeof($arra);
    for ($i = 0; $i < $size; $i++){
      $numMean+=$arra[$i];
    }
    $numMean/=$size;
    
    return $numMean;
}

function median($arra) {
   
    $size=sizeof($arra);
    sort($arra);
    $midLevel = floor(($size-1)/2);
    $median = $arra[$midLevel];
    
    /* For more complex arrays
    if($size % 2) { 
        $median = $arra[$midLevel];
    }else { 
        $low = $arr[$midLevel];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }*/
    return $median;
}

function table($arra) {
    $size=sizeof($arra);
    $m=mean($arra);
    $med=median($arra);
    
    echo '<table class="bordered">';
        echo "<thead>";
            echo "<tr>";
                echo"<th>Ascending sorted values</th>";
                echo"<th>Descending sorted values</th>";
            echo "</tr>";
        echo "</thead>";
        
        echo "<tbody>";
            for ($i = 0; $i < $size; $i++){
            echo "<tr>";
                sort($arra);
                echo "<td>$arra[$i]</td>";
                rsort($arra);
                echo "<td>$arra[$i]</td>";
            echo "</tr>";
            };
        echo "</tbody>";
    echo "</table></br>";
        
        //Segunda Tabla
        echo "<table>";
            echo "<thead>";
                echo "<tr>";
                    echo"<th>Mean</th>";
                    echo"<th>Median</th>";
                echo "</tr>";
            echo "</thead>";
            
            echo "<tbody>";
                echo "<tr>";
                    echo "<td>$m</td>";
                    echo "<td>$med</td>";
                echo "</tr>";
            echo "</tbody>";
        echo "</table>" ;
}


function area($r){
    $a=$r*$r*M_PI;
    return $a;
}

?>