//Java Script

function precio() {
    'use strict';
    var total, nviolin = document.getElementById("org").value, npiano = document.getElementById("org2").value, nbass = document.getElementById("org3").value, ps = /[0-9]/, flag, flag2, flag3;
    flag = nviolin.match(ps);
    flag2 = npiano.match(ps);
    flag3 = nbass.match(ps);
    
    if (!flag || !flag2 || !flag3 || nviolin > 99 || npiano > 99 || nbass > 99) {
        window.alert("Inserte una cantidad valida (max. 99) ");
    } else {
        total = "$" + (nviolin * 8000 + npiano * 35000 + nbass * 14000) * 1.16;
        //window.alert("Total: " + total);
        document.getElementById("tot").value = total;
    }
    
}
function mOver(obj) {
    'use strict';
    obj.innerHTML = "Precio No Incluye IVA";
}

function mOut(obj) {
    'use strict';
    obj.innerHTML = "";
}

function allowDrop(ev) {
    'use strict';
    ev.preventDefault();
}

function drag(ev) {
    'use strict';
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    'use strict';
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

    