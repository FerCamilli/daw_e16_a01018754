<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/hello/{name}', function ($name) {
    return 'Hello '.$name.'!';
});


$app->get('/hello/{name}/estatura/{estatura}/peso/{peso}', function ($estatura, $peso) {
    
     $imc = $peso / ($estatura * $estatura);
    
    return 'Tu imc es '.$imc;
});
    


$app->run();
?>