<?php

include('_header.html');
?>


<div class="container">
    <h1>Laboratorio 20</h1>

    
    <?php
    $url = "https://daw-fercamilli.c9users.io/Lab20/Lab20_servicio/silex/web/index.php/hello/fer"; //Route to the REST web service
    $c = curl_init($url);
    $response = curl_exec($c);
    curl_close($c);
    #var_dump($response); 
    #die();
    ?>
    
    
    
    <form action="control.php" method="get">
        <label>Peso: </label>
        <input type="text" name="peso"/>
        
        <label>Estatura: </label>
        <input type="text" name="estatura"/>
        <br />
        
        <input type="submit" name="submit" value="Enviar"/>
    </form>
    
    
    
    
    <div class="row">
      <div class="col s12 5">
        <div class="card-panel teal">
          <span class="white-text">
            
            <h5>Preguntas</h5>
            <p>¿A qué se refiere la descentralización de servicios web?</p>
                <p>Se refiere a tener un servicio web localizado en un servidor con varias copias del mismo en otros servidores para poder atender grandes 
                numeros de peticiones, o cuando hay problemas con el servidor principal.</p></br>
            <p>¿Cómo puede implementarse un entorno con servicios web disponibles aún cuando falle un servidor?</p>
                <p>Precisamente descentralizando dicho servicio, teniendo el servicio del lado del servidor con varios respaldos corriendo en otros 
                servidores de manera que puedas tener tu servicio del lado del cliente corriendo y haciendo peticiones al servidor que funcione.</p></br>
           
          </span>
        </div>
      </div>
    </div>
</div>



<?php
include('_footer.html');
?>